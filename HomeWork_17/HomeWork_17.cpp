﻿// HomeWork_17.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <cmath>

class TestClass
{
private:
    int a;
    int b;
    int c;
public:
    TestClass()
    {
        a = 0;
        b = 1;
        c = 2;
    }
    TestClass(int NewA, int NewB, int NewC)
    {
        a = NewA;
        b = NewB;
        c = NewC;
    }
    int GetA()
    {
        a = b+c;
        return a;
    }
    int GetB()
    {
        b = a+c;
        return b;
    }
    int GetC()
    {
        c = a+b;
        return c;
    }
};

class Vector
{
private:
    //Координаты начала вектора
    double x;
    double y;
    double z;
    //Координаты конца вектора
    double x1;
    double y1;
    double z1;
public:
    Vector():x(0),y(0),z(0),x1(0),y1(0),z1(0)
    {}

    Vector(double _x, double _y, double _z, double _x1, double _y1, double _z1) :x(_x), y(_y), z(_z),x1(_x1), y1(_y1), z1(_z1)
    {}
    // Печать координат начала вектора и координат конца вектора
    void Show()
    {
        std::cout <<" \n"<< x << " " << y << " " << z;
        std::cout << " \n" << x1 << " " << y1 << " " << z1;
    }
    // Вычисление квадрата разности координат
    double Pow(double a, double b)
    {
        int k = 0;
        int n = 2;
        double d = 1;
        while (k!=n)
        {
            k++;
            d *= (a - b);
        }
        return d; 
    }
    // Вычисление длины вектора
    double  Lenght()
    {
        double A;
        double B;
        //double C;
        A = Pow(x, x1) + Pow(y, y1) + Pow(z, z1);
        B = sqrt(A);
       /* C = abs(B);*/
        std::cout << "\n"<<B;
        return B;
    }
};

//Если делать вычисление длины вектора через два класса
class Vector1
{
private:
    //Координаты начала вектора
    double x;
    double y;
    double z;
public:
    Vector1() :x(0), y(0), z(0)
    {}
    Vector1(double _x, double _y, double _z) :x(_x), y(_y), z(_z)
    {}
    // Печать координат начала вектора и координат конца вектора
    void Show()
    {
        std::cout << " \n" << x << " " << y << " " << z;
    }
    int GetX()
    {
        return x;
    }
    int GetY()
    {
        return y;
    }
    int GetZ()
    {
        return z;
    }
};

// Вычисление квадрата разности координат
double Pow(double a, double b)
{
    int k = 0;
    int n = 2;
    double d = 1;
    while (k != n)
    {
        k++;
        d *= (a - b);
    }
    return d;
}

// Вычисление длины вектора
double Lenght(double x, double x1, double y, double y1, double z,double z1)
{
    double A;
    double B;
    //double C;
    A = Pow(x, x1) + Pow(y, y1) + Pow(z, z1);
    B = sqrt(A);
    /* C = abs(B);*/
    std::cout << "\n" << B;
    return B;
}
int main()
{
    TestClass temp(10,15,20);
    std::cout << temp.GetA()<<" "<<temp.GetB()<<" "<<temp.GetC();
    Vector v(100, 200, 300,200,300,400);
    v.Show();
    v.Lenght();
    Vector1 vec1(100, 200, 300);
    Vector1 vec2(300, 100, 200);
    Lenght(vec1.GetX(), vec2.GetX(), vec1.GetY(), vec2.GetY(), vec1.GetZ(), vec2.GetZ());

}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
